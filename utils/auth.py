from utils.exceptions import *
from hashlib import sha256
from sanic_jwt import exceptions

class auth(object):
    def __init__(self, db):
        self.db = db
    async def authenticate(self, request, *args, **kwargs):
        user = request.json.get("username")
        password = request.json.get("password") 

        try:
            user_info = self.db.get_user_login_information(user)
            if (len(user_info) == 0):
                raise exceptions.AuthenticationFailed("Username is incorrect")
        except Exception as e:
            raise e

        if not user or not password:
            raise exceptions.AuthenticationFailed("Missing username or password.")

        if not user_info[3]:
            raise exceptions.AuthenticationFailed("User is disabled.")

        if user_info[0] != sha256(bytes(password, encoding='utf-8')).hexdigest():
            raise exceptions.AuthenticationFailed("Password is incorrect.")
        
        return {'user':user, 'user_id':user_info[1], 'roles':[user_info[2]]}

    async def getuser(self, request, payload, *args, **kwargs):
        if payload:
            try:
                user_id = payload.get('user_id', None)
                user = self.db.get_user(user_id)[0]
                return user
            except:
                return None
        else:
            return None

    def check_api_key(self, payload):
        user_id = payload.get("user_id")
        key = payload.get("key")
        db_secret = self.db.get_api_key_user(user_id)[0]
        #the keys are prevalidated using the system secret, if they are altered then they need the same key

        if not key and not db_secret:
            return True
        elif key and not db_secret:
            return False
        elif key and db_secret and key == db_secret:
            return True
        else:
            return True

    def scope(self, user, *args, **kwargs):
        return user['roles']
