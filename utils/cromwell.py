import re
import json
import logging
import webbrowser
from time import sleep
from collections.abc import Iterable
from itertools import repeat
import requests
import asyncio
from requests.auth import HTTPBasicAuth


class Cromwell:
    """Wrapper for the Cromwell REST API"""

    def __init__(self, cromwell_url, username=None, password=None, api_version='v1'):
        """API wrapper for a running cromwell server

        :param str cromwell_url: url of a running cromwell instance
        :param str | None username: (optional) username for the cromwell instance
        :param str | None password: (optional) password for the cromwell instance
        :param str api_version: version of the cromwell API
        """
        self.logger = logging.getLogger(__name__)
        if isinstance(cromwell_url, str):
            self._cromwell_url = cromwell_url
        else:
            raise TypeError('cromwell_url must be a str, not %s' % type(cromwell_url))

        if isinstance(username, str) or username is None:
            self.username = username
        else:
            raise TypeError('If provided, username must be a str, not %s' % type(username))

        if isinstance(password, str) or username is None:
            self.password = password
        else:
            raise TypeError('If provided, password must be a str, not %s' % type(password))

        if isinstance(api_version, str):
            self.api_version = api_version
        else:
            raise ValueError('version must be a str, not %s' % type(api_version))

        self.auth = HTTPBasicAuth(username, password) if username and password else None
        self.url_prefix = '{cromwell_url}/api/workflows/{version}'.format(
            cromwell_url=self.cromwell_url, version=self.api_version)

        # check that server is running
        #if not self.server_is_running():
        #    raise RuntimeError('url, username, and password did not authenticate to a running '
        #                       'cromwell instance.')

    def __repr__(self):
        return '<Cromwell Server: ip: %s>' % self.cromwell_url

    @property
    def cromwell_url(self):
        """URL for the cromwell REST endpoints."""
        return self._cromwell_url

    @cromwell_url.setter
    def cromwell_url(self, value):
        if not re.match('https?://', value):
            raise ValueError('cromwell_url must be an http or https address.')
        if not self.server_is_running(value):
            raise ValueError('cromwell_url does not match a running cromwell server')
        self._cromwell_url = value.rstrip('/')  # trailing slash is not accepted by cromwell



    async def swagger(self):
        """Open the swagger page for this cromwell server."""
        async_request = asyncio.coroutine(requests.get)
        return async_request(self.cromwell_url + "/swagger")
        #webbrowser.open(self.cromwell_url)

    
    async def server_is_running(self, *args, **kwargs):
        """Return True if the server is running, else False.

        :param bool verbose: if True, print the query, response code, and content (default False)
        :param bool open_browser: if True, display the GET result in browser (default False)
        """
        response = await self.get(self.cromwell_url, *args, **kwargs)
        return True if response else False

    async def get(self, url, verbose=False, open_browser=False, *args, **kwargs):
        """Make a REST GET query to url.

        :param str url: GET query url

        :param bool verbose: if True, print the query, response code, and content (default False)
        :param bool open_browser: if True, display the GET result in browser (default False)
        :param args: additional positional args to pass to requests.get
        :param kwargs: additional keyword args to pass to request.get
        :return requests.Response: requests response object
        """
        async_request = asyncio.coroutine(requests.get)
        response = await async_request(url, auth=self.auth, *args, **kwargs)
        if response.status_code in [201, 200]:
            try:
                return response.json()
            except Exception as e:
                return True
        else:
            raise Exception(response.text)
        if verbose:
            self.print_request('GET', url, response)
        # if open_browser:
        #     webbrowser.open(url)

    async def post(self, url, wf_id=None, files=None, method=None, *args, **kwargs):
        """Make a REST GET query to url.

        :param str url: GET query url

        :param bool verbose: if True, print the query, response code, and content (default False)
        :param bool open_browser: if True, display the GET result in browser (default False)
        :param args: additional positional args to pass to requests.get
        :param kwargs: additional keyword args to pass to request.get
        :return requests.Response: requests response object
        """
        async_request = asyncio.coroutine(requests.post)
        response = await async_request(url, auth=self.auth, files=files)
        print(response.status_code)
        if response.status_code in [201, 200]:
            return response.json()
        else:
            raise Exception(response.text)

    async def submit(self, *args):
        
        url = '{cromwell_url}/api/workflows/{version}'.format(
            cromwell_url=self.cromwell_url, version=self.api_version)

        return await self.post(url, files=args[0])

    async def version(self, *args, **kwargs):
        """Retrieve the cromwell version

        :param bool verbose: if True, print the query, response code, and content (default False)
        :param bool open_browser: if True, display the GET result in browser (default False)
        :param args: additional positional args to pass to requests.get
        :param kwargs: additional keyword args to pass to request.get
        :return response.Response: requests response object
        """
        url = '{cromwell_url}/engine/{version}/version'.format(
            cromwell_url=self.cromwell_url, version=self.api_version)
        return await self.get(url, *args, **kwargs)

    async def stats(self, *args, **kwargs):
        """Retrieve cromwell statistics on number of running jobs

        :param bool verbose: if True, print the query, response code, and content (default False)
        :param bool open_browser: if True, display the GET result in browser (default False)
        :param args: additional positional args to pass to requests.get
        :param kwargs: additional keyword args to pass to request.get
        :return response.Response: requests response object
        """
        url = '{cromwell_url}/engine/{version}/stats'.format(
            cromwell_url=self.cromwell_url, version=self.api_version)
        return await self.get(url, *args, **kwargs)

    async def status(self, workflow_id, *args, **kwargs):
        """Retrieve status for workflow_id.
        :param str workflow_id: hash for workflow to abort
        :param bool verbose: if True, print the query, response code, and content (default False)
        :param bool open_browser: if True, display the GET result in browser (default False)
        :param args: additional positional args to pass to requests.get
        :param kwargs: additional keyword args to pass to request.get
        :return response.Response: requests response object
        """
        url = self.url_prefix + '/{id}/status'.format(id=workflow_id)
        return await self.get(url, *args, **kwargs)

    async def abort_workflow(self, workflow_id, *args, **kwargs):
        """Abort a workflow.
        :param str workflow_id: hash for workflow to abort
        :param bool verbose: if True, print the query, response code, and content (default False)
        :param args: additional arguments to pass to requests.post
        :param kwargs: additional arguments to pass to requests.post
        :return response.Response: requests response object
        """
        url = self.url_prefix + '/{id}/abort'.format(id=workflow_id)
        return await self.post(url, *args, **kwargs)

# Needs some fixing to support better queries
    async def query(self, query_dict=None):
        url = '{cromwell_url}/api/workflows/{version}/query'.format(
            cromwell_url=self.cromwell_url, version=self.api_version)

        return await self.post(url, data=query_dict)

    async def output_files(self, workflow_id, *args, **kwargs):
        url = self.url_prefix + '/{id}/outputs'.format(
            id=workflow_id
        )
        return await self.get(url, *args, **kwargs)

    async def metatdata(self, workflow_id):
        url = self.url_prefix + '/{id}/metadata'.format(
            id=workflow_id
        )

        return await self.get(url)

    # reuqest.files.get('file')
    # file_parameters = {
    #    'body': test_file.body, # binary
     #   'name': test_file.name,
    #    'type': test_file.type,
    #}