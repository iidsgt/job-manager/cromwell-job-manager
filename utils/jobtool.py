import argparse
import configparser
from getpass import getpass
from os import path
import requests
import sys


class API_Interact(object):
    def __init__(self, url, user, u_pass):
        self.url = url
        self.username = user
        self.password = u_pass
        self.token = ''
        self.__reinitialize_token()

    def __reinitialize_token(self):
        if self.password:
            r = requests.post(self.url + '/v1/auth', json={'username': self.username, 'password': self.password})
            if r.status_code == 200:
                self.token = r.json()['access_token']
                self.password = None
            else:
                print("Authentication Failed")
                return False
            
        else:
            print("Authorization has expired.")
            password = getpass("Password: ")
            r = requests.post(self.url + '/v1/auth', json={'username': self.username, 'password': password})
            if r.status_code == 200:
                self.token = r.json()['access_token']
            else:
                print("Authentication Failed")
                return False
        return True
    def __test_connection(self):
        r = requests.get(self.url + '/v1/auth/me', headers={'Authorization': 'Bearer ' + self.token})
        if r.status_code == 200:
            return True
        else:
            return False

    def submit_workflow(self, cwl, inputs):
        if not self.__test_connection():
            if not self.__reinitialize_token():
                print('Failure to authorize')
                sys.exit(1)
        files = {
            'workflow':('cwl', open(cwl, 'rb')),
            'inputs':('inputs', open(inputs, 'rb'))
        }
        r = requests.post(self.url + '/v1/jobs/submitjob', headers={'Authorization': 'Bearer ' + self.token}, files=files)
        if r.status_code == 200:
            j = r.json()['job_id']
            print(f'Workflow Submitted with ID: {j}')
            sys.exit(0)
        else:
            f = r.text
            print(f'Workflow Submission Failed: {f}')
            sys.exit(1)

    def check_workflow_status(self, jobid):
        if not self.__test_connection():
            if not self.__reinitialize_token():
                print('Failure to authorize')
                sys.exit(1)
        r = requests.get(self.url + f'/v1/jobs/getstatus/{jobid}', headers={'Authorization': 'Bearer ' + self.token})
        if r.status_code == 200:
            j = r.json()['status']
            print(f'Job Status: {j}')
            sys.exit(0)
        else:
            f = r.text
            print(f'Command Failed {f}')
            sys.exit(1)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', dest='config_file', action='store', help='Specify location to config file.')
    subparsers = parser.add_subparsers(dest="subcommand", title="Valid Commands")
    submit_parser = subparsers.add_parser('submit')
    submit_parser.add_argument('--cwl', dest='cwl', action='store', help='Specify the location to the cwl file you wish to submit.', required=True)
    submit_parser.add_argument('--inputs', dest='inputs', action='store', help='Specify the location of the inputs yaml.', required=True)

    status_parser = subparsers.add_parser('status')
    status_parser.add_argument('--jobid', '-j', dest='jobid', action='store', help='Specify the job id you want to check the status for', required=True)

    return parser




def main():
    parser = parse_args()
    args = parser.parse_args()
    if args.subcommand == None:
        parser.print_help()
        sys.exit(0)
        
    config = configparser.ConfigParser()
    configpath = path.join(path.expanduser("~"), '.jobtool.ini')
    if args.config_file:
        config.read(args.config_file)
    elif path.exists(configpath) and not args.config_file:
        try:
            config.read(configpath)
        except:
            print("Config File Couldn't Be Read")
    else:
        decision = input("Would you like to create a new config file? [y/n]: ")

        if decision in ['y', 'Y']:
            url = input("Specify Job Manager URL: ")
            username = input("Specify your Username: ")
            with open(configpath, 'w') as f:
                config['DEFAULT']['user'] = username
                config['DEFAULT']['url'] = url
                config.write(f)
        else:
            sys.exit(0)



    if args.subcommand == "submit":
        if args.cwl == None or args.inputs == None:
            print("Error Inputs or CWL file not specified.")
            sys.exit(0)
        u_pass = getpass("Password: ")
        api = API_Interact(u_pass=u_pass, user=config['DEFAULT']['user'], url=config['DEFAULT']['url'])
        api.submit_workflow(cwl=args.cwl, inputs=args.inputs)

    if args.subcommand == 'status':
        if args.jobid ==None:
            print('Error Job ID is required')
            sys.exit(0)
        u_pass = getpass("Password: ")
        api = API_Interact(u_pass=u_pass, user=config['DEFAULT']['user'], url=config['DEFAULT']['url'])
        api.check_workflow_status(jobid=args.jobid)
        
    else:
        args.print_help(sys.stderr)
        sys.exit(0)

if __name__ == "__main__":
    main()
