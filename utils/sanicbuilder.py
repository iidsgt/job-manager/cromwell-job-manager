from urllib.parse import urlparse, urljoin
from utils import exceptions
import sys
import inspect
import crayons


    
def create_routes(app, *args, params=None, logger=None):

    def _split_mangle(mangle):
        x = mangle.split('__')
        x[0] = x[0].split('_')[1]
        return {"route_group":x[0], "group_url_name":x[0].lower(), "method_url_name":x[1].lower(), "handler_function":"__" + x[1]}

    def _uri_validator(a,b):
        x = "/" + a + "/" + b + "/"
        try:
            result = urlparse(x)
            return all([result.scheme, result.netloc, result.path])
        except:
            return False

    def _build_url(arguments, base, group, method):
        arguments = ["<" + s + ">" for s in arguments]
        if not method == "":
            arguments.insert(0, method)
            pass
        arguments.insert(0, group)
        return base + "/" + "/".join(arguments)


    if len(args) < 1:
        raise exceptions.FunctionArgumentError(create_routes)
        sys.exit()
    

    for RouteClass in args:
        if "init" not in RouteClass.__dict__:
                raise exceptions.ClassNotInitializedError
                sys.exit(1)


        RouteClass.init(params)
        
        sub_route_groups = []
    
        for method in dir(RouteClass):
            if method[0] == "_" and method[1] != "_":
                
                if logger:
                    logger.debug(crayons.green("Found Route Class %s" % (method)))
                else:
                    print(crayons.green("Found Route Class %s" % (method)))
                
                temp_method = _split_mangle(method)

                #if _uri_validator(temp_method["route_group"], temp_method["url_name"]):
                    #sub_route_groups.append(temp_method)

                #else:
                    #raise exceptions.RouteNameError(temp_method)
                    #sys.exit()
                sub_route_groups.append(temp_method)

        for route in sub_route_groups:


            route_class_name = "_" + route["route_group"] + route["handler_function"]
            route_class = RouteClass.__dict__[route_class_name]

            for method in dir(route_class):
                #print(method)
                if method[0] != "_" and method[1] != "_" and method in ['get', 'put', 'post', 'patch', 'delete', 'options']:

                    arguments = inspect.getargspec(route_class.__dict__[method]).args
                    #print(arguments)

                    if 'self' not in arguments:
                        raise exceptions.RouteArgumentError
                        sys.exit()

                    elif route["handler_function"] == "__root":
                        route["method_url_name"] = ""
                        del arguments[0 : 2]

                    elif len(arguments) == 2:
                        arguments = []

                    else:
                        #print(route_class)
                        del arguments[0 : 2]
                    
                    url = _build_url(arguments=arguments, base=RouteClass.base, group=route["group_url_name"], method=route["method_url_name"])
                    if logger:
                        logger.debug(crayons.blue("Adding route %s at url %s with params %s" % (route["route_group"], url, "|".join(arguments))))
                    else:
                        print(crayons.blue("Adding route %s at url %s with params %s" % (route["route_group"], url, "|".join(arguments))))
                    app.add_route(route_class.as_view(), url)

                else:
                    continue

    return None