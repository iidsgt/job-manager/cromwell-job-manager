/*DB app {
    table users {
        VARCHAR(64) password,
        VARCHAR(25) username,
        VARCHAR(254) email,
        ENUM ["admin", "user"] role,
        VARCHAR(64) api_key,
        VARCHAR(64) user_id
    }
    table running_tasks {
       VARCHAR(64) user_id FOREIGN KEY users.user_id
       VARCHAR(28) job_id,
       ENUM ["running", "canceled", "failed"] status
    }

    table completed_tasks
}*/
/*App DB And Table*/
CREATE DATABASE appdb;
CREATE USER jobmanageruser WITH ENCRYPTED PASSWORD "ndjsalnfjdsnfjdsklnfdjsknfdjksfndjklfd";
GRANT CONNECT on DATABASE appdb to jobmanageruser;

CREATE TYPE role AS ENUM ('admin', 'user');
CREATE TYPE status as ENUM ('Running', 'Canceled', 'Failed', 'Succeeded', 'Submitted', 'Aborting');

CREATE TABLE users (
    username VARCHAR(25) UNIQUE NOT NULL,
    password VARCHAR(64) NOT NULL,
    email VARCHAR(254) UNIQUE NOT NULL,
    role role NOT NULL DEFAULT 'user',
    api_key VARCHAR(64),
    user_id VARCHAR(64) UNIQUE NOT NULL,
    _id SERIAL PRIMARY KEY NOT NULL,
    is_enabled BOOLEAN NOT NULL DEFAULT true
);

CREATE TABLE task_history (
    user_id VARCHAR(64) NOT NULL FOREIGN KEY users.user_id,
    job_id VARCHAR(36) NOT NULL UNIQUE,
    status status NOT NULL,
    cwl jsonb NOT NULL,
    inputs jsonb NOT NULL,
    created_at TIMESTAMP NOT NULL,
    completed_at TIMESTAMP
);
/* for Future use*/
CREATE TABLE projects (
    project_id VARCHAR(64) PRIMARY KEY UNIQUE,
    users VARCHAR(64)[] ELEMENT REFERENCES users.user_id,
    project_name VARCHAR(25) NOT NULL UNIQUE,
    project_assets 
)

GRANT SELECT, UPDATE, DELETE, INSERT on TABLE users TO jobmanageruser;
GRANT SELECT, UPDATE, DELETE, INSERT on TABLE task_history TO jobmanageruser;

INSERT INTO users (username, password, email, role, user_id) 
VALUES ('test', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'mailxxx@mail.com', 'admin', '8b0d8162-f8e3-499c-a542-a5cfaaeb0450'

/*Files Db Schema*/

