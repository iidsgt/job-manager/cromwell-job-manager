import crayons
import inspect
import logging

class Error(Exception):
    @classmethod
    def init(self, logger):
        self.logger = logging.getLogger(__name__)
        """Base class for other exceptions"""
        pass

class RouteNameError(Error):
    def __init__(self, arg):
        Error.logger.error("There is an issue with the name of %s" % (arg))
        pass

class RouteArgumentError(Error):
    def __init__(self, arg):
        Error.logger.error("There is an issue with the arguments in function %s" % (arg))
        pass

class FunctionArgumentError(Error):
    def __init__(self, fn):
        Error.logger.error("An incorrect number of arguments was specified")
        Error.logger.error("Function %s expects %s" % (fn.__name__, inspect.getargspec(fn).args))

class DatabaseConnectionError(Error):
    def __init__(self, arg):
        Error.logger.error(crayons.red("🤬  Error Generating Connection Pool: Please check credentials & permissions and try again."))

class ClassNotInitializedError(Error):
    def __init__(self, arg):
        Error.logger.error("Class %s does not have init function" % (arg))