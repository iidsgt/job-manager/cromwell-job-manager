import requests
import logging
from config.environment_helper import EnvUtil



class gitlab_interact:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.db_name = EnvUtil.get('db_name')
        self.gitlab_url = EnvUtil.get('gitlab_biocwl_url')
        self.gitlab_token = EnvUtil.get('gitlab_token')
        self.files = {}


    def get_files(self, cwl_type):
        try:
            files_list = requests.get(url=self.gitlab_url+'tree?path='+cwl_type,
                                      headers={"PRIVATE-TOKEN": self.gitlab_token})
            for file_object in files_list.json():
                if file_object['type'] == 'blob':
                    self.files.update({file_object['name']:file_object['path']})
            return self.files
        except Exception as e:
            raise e

    def download_files(self, path):
        try:
            cwl_file = requests.get(
                url=f"{self.gitlab_url}files/{path}/raw?ref=master",
                headers={"PRIVATE-TOKEN": self.gitlab_token}, stream=True)
            return cwl_file.content
        except Exception as e:
            raise e

