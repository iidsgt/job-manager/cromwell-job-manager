from fs.osfs import OSFS
from config.environment_helper import EnvUtil
import logging


class FileManager:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.home_fs = OSFS(EnvUtil.get('home_dir'))
        
            
    def list_files(self):
        files = self.home_fs.listdir('/')
        return files