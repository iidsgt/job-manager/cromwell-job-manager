import asyncio
import datetime
import logging
import crayons
from utils.cromwell import Cromwell as cwm
from config.environment_helper import EnvUtil
import traceback



async def db_syncronization(db):
    await asyncio.sleep(90)
    logger = logging.getLogger(__name__)
    port = EnvUtil.get('cromwell_port')
    cromwell_domain = EnvUtil.get('cromwell_domain')
    cromwell_url = f'{cromwell_domain}:{port}'
    cromwell_config = {'cromwell_url': cromwell_url}

    try:
        cw = cwm(**cromwell_config)
    except Exception as e:
        logger.error(crayons.red(e))

    while True:
        await asyncio.sleep(30)
        try:
            db_jobs = db.get_running_tasks()
        except Exception as e:
            logger.error(e)
            await asyncio.sleep(10)
            continue


        for job in db_jobs:
            status = await cw.server_is_running()
            if status:
                try:
                    cw_job_status = await cw.status(job[0])
                except:
                    if job[1] != 'Failed':
                        logger.error("Job not in cromwell: %s, removing" % (job[0]))
                        db.update_task(job_id=job[0], status='Failed', timestamp=datetime.datetime.utcnow().isoformat())
                    continue

                if job[1] != cw_job_status['status']:
                    if cw_job_status['status'] == 'running':
                        try:
                            db.update_task(status=cw_job_status['status'], job_id=job[0])
                        except Exception as e:
                            logger.error(e)
                    else:
                        try:
                            db.update_task(status=cw_job_status['status'], job_id=job[0], timestamp=datetime.datetime.utcnow().isoformat())
                        except Exception as e:
                            logger.error(e)
                else:
                    continue