from os import path
import psycopg2.pool
import psycopg2, sys, crayons
import psycopg2.extras
import psycopg2.errorcodes
from psycopg2 import pool, extras
from utils.exceptions import *
import logging
from config.environment_helper import EnvUtil
from uuid import uuid4
from psycopg2 import pool, extras
from sys import modules
from yoyo import read_migrations
from yoyo import get_backend


class db_interact:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.db_name = EnvUtil.get('db_name')
        self.user = EnvUtil.get('db_user')
        self.password = EnvUtil.get('db_password')
        self.host = EnvUtil.get('db_host')
        self.port = EnvUtil.get('db_port')
        self.base_dir = path.dirname(modules['__main__'].__file__)

        def prepare_migrations():
            backend = get_backend(f'postgres://{self.user}:{self.password}@{self.host}/{self.db_name}')
            migrations = read_migrations(path.join(self.base_dir, 'migrations'))
            with backend.lock():
                self.logger.info("Running Migrations")
                backend.apply_migrations(backend.to_apply(migrations))
        try:
            prepare_migrations()
        except Exception as e:
            self.logger.error(e)
            
        try:
            self.logger.info(crayons.green("Initializing Database Connection & Threadpool"))
            self.cpool = psycopg2.pool.SimpleConnectionPool(1, 20, user=self.user,
                                                                    password=self.password,
                                                                    host=self.host,
                                                                    port=self.port,
                                                                    dbname=self.db_name)
            if(self.cpool):
                self.logger.info(crayons.green("Connection pool created successfully"))
                self.check_or_initialize_db()
        except:
            raise DatabaseConnectionError
            sys.exit()
            
    def check_or_initialize_db(self):
        conn = self.cpool.getconn()
        def create_db(conn):
            self.logger.info(crayons.yellow("Database Empty: Creating Tables"))
            cursor = conn.cursor()
            try:
                cursor.execute("CREATE TYPE role AS ENUM ('admin', 'user')", ())
            except Exception as e:
                self.logger.warning(crayons.yellow(e))
                conn.rollback()
                pass
            try:
                cursor.execute("CREATE TYPE status as ENUM ('Running', 'Canceled', 'Failed', 'Succeeded', 'Submitted', 'Aborting')")
            except Exception as e:
                self.logger.warning(crayons.yellow(e))
                conn.rollback()
                pass
            try:
                cursor.execute("""
                CREATE TABLE users (
                username VARCHAR(25) UNIQUE NOT NULL,
                password VARCHAR(64) NOT NULL,
                email VARCHAR(254) UNIQUE NOT NULL,
                role role NOT NULL DEFAULT 'user',
                api_key VARCHAR(320),
                user_id VARCHAR(64) UNIQUE NOT NULL,
                _id SERIAL PRIMARY KEY NOT NULL,
                is_enabled BOOLEAN NOT NULL DEFAULT true
                )
                """, ())
            except Exception as e:
                self.logger.warning(crayons.yellow(e))
                conn.rollback()
                pass
            try:
                cursor.execute("""
                CREATE TABLE task_history (
                user_id VARCHAR(64) REFERENCES users(user_id),
                job_id VARCHAR(36) NOT NULL UNIQUE,
                status status NOT NULL,
                cwl jsonb NOT NULL,
                inputs jsonb NOT NULL,
                file_mapping jsonb NOT NULL,
                created_at TIMESTAMP NOT NULL,
                completed_at TIMESTAMP
                )
                """, ())
            except Exception as e:
                self.logger.warning(crayons.yellow(e))
                conn.rollback()
                pass
            try:
                cursor.execute("""
                INSERT INTO users (username, password, email, role, user_id) 
                VALUES ('test', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'mailxxx@mail.com', 'admin', '8b0d8162-f8e3-499c-a542-a5cfaaeb0450')
                """)
            except Exception as e:
                self.logger.warning(crayons.yellow(e))
                conn.rollback()
                pass
            finally:
                cursor.close()
                conn.commit()
                self.cpool.putconn(conn)
                self.logger.info(crayons.green("Database Create Sucessfully"))
            return None


        cursor = conn.cursor()
        try:
            cursor.execute("select exists(select * from information_schema.tables where table_name=%s)", ('users',))
            if not cursor.fetchone()[0]:
                cursor.close()
                create_db(conn)
                return None
            cursor.execute("select exists(select * from information_schema.tables where table_name=%s)", ('task_history',))
            if not cursor.fetchone()[0]:
                cursor.close()
                create_db(conn)
                return None

            cursor.close()
            self.cpool.putconn(conn)
            return None

        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            raise e

    #Login/User ACL Functions

    def get_user_login_information(self, username):
        conn = self.cpool.getconn()
        cursor = conn.cursor()
        try:
            cursor.execute("SELECT password, user_id, role, is_enabled from users WHERE username = %s", (username,))
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e

        rows = cursor.fetchone()
        cursor.close()
        self.cpool.putconn(conn)
        return rows
    
    def create_user(self, username, password, email, role, api_key=None):
        conn = self.cpool.getconn()
        cursor = conn.cursor()
        try:
            uuid = str(uuid4())
            cursor.execute("INSERT INTO users (username, password, email, role, api_key, user_id) VALUES (%s, %s, %s, %s, %s, %s);", (username, password, email, role, api_key, uuid,))
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            if (e.pgcode == psycopg2.errorcodes.UNIQUE_VIOLATION):
                cursor.close()
                self.cpool.putconn(conn)
                raise Exception("Username or Email Already Exists")
                
            else:
                cursor.close()
                self.cpool.putconn(conn)
                raise e
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return uuid
    
    def list_all_users(self):
        conn = self.cpool.getconn()
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        try:
            cursor.execute("SELECT username, email, role, user_id FROM users") 
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e

        rows = cursor.fetchall()
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return rows

    def get_user(self, user_id):
        conn = self.cpool.getconn()
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        try:
            cursor.execute("SELECT username, email, role, user_id FROM users WHERE user_id = %s", (user_id,)) 
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e

        rows = cursor.fetchall()
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return rows

    def remove_user(self, user_id):
        conn = self.cpool.getconn()
        cursor = conn.cursor()
        try:
            cursor.execute("UPDATE users SET is_enabled = false WHERE user_id = %s", (user_id,)) #Add user checking for deletion
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        
        return cursor.rowcount

    def set_api_key_user(self, user_id, key):
        conn = self.cpool.getconn()
        cursor = conn.cursor()
        try:
            cursor.execute("UPDATE users SET api_key = %s WHERE user_id = %s", (key, user_id,)) #Add user checking for deletion
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        
        return cursor.rowcount

    def get_api_key_user(self, user_id):
        conn = self.cpool.getconn()
        cursor = conn.cursor()
        try:
            cursor.execute("SELECT api_key from users WHERE user_id = %s AND is_enabled = true", (user_id,)) #Add user checking for deletion
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e
        rows = cursor.fetchone()
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        
        return rows

    def remove_api_key_user(self, user_id):
        conn = self.cpool.getconn()
        cursor = conn.cursor()
        try:
            cursor.execute("UPDATE users SET api_key = '' WHERE user_id = %s", (user_id,)) #Add user checking for deletion
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        
        return cursor.rowcount

    #Task Functions

    def create_task(self, user_id, job_id, cwl, inputs, mapping, created_at, status):
        conn = self.cpool.getconn()
        cursor = conn.cursor()
        try:
            cursor.execute("INSERT INTO task_history (user_id, job_id, cwl, inputs, file_mapping, created_at, status) VALUES (%s, %s, %s, %s, %s, %s, %s);", (user_id, job_id, cwl, inputs, mapping, created_at, status,))
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return None

    def get_task_information(self, job_id):
        conn = self.cpool.getconn()
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        try:
            cursor.execute("SELECT * from task_history WHERE job_id = %s", (job_id,))
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e
        data = cursor.fetchone()
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return data

    def get_batch_task_information(self, job_ids, user_id):
        conn = self.cpool.getconn()
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        try:
            cursor.execute("SELECT job_id, status from task_history WHERE job_id = ANY(%s) AND user_id = %s", (job_ids, user_id,))
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e
        data = cursor.fetchall()
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return data

    def get_tasks_by_user(self, user_id):
        conn = self.cpool.getconn()
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        try:
            cursor.execute("SELECT * from task_history WHERE user_id = %s ORDER BY created_at DESC", (user_id,))
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            cursor.close()
            self.cpool.putconn(conn)
            raise e
        data = cursor.fetchall()
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return data


    def update_task(self, status, job_id, timestamp=None):
        conn = self.cpool.getconn()
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        if timestamp:
            try:
                cursor.execute("UPDATE task_history SET status = %s, completed_at = %s  WHERE job_id = %s", (status, timestamp, job_id,))
            except psycopg2.Error as e:
                self.logger.error(crayons.red(e.pgerror))
                cursor.close()
                self.cpool.putconn(conn)
                raise e
        else:
            try:
                cursor.execute("UPDATE task_history SET status = %s WHERE job_id = %s", (status, job_id,))
            except psycopg2.Error as e:
                self.logger.error(crayons.red(e.pgerror))
                cursor.close()
                self.cpool.putconn(conn)
                raise e
            
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return cursor.rowcount

    def get_running_tasks(self):
        conn = self.cpool.getconn()
        cursor = conn.cursor()
        try:
            cursor.execute("SELECT job_id, status from task_history WHERE status = 'Submitted' OR status = 'Running' ")
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            raise e
        data = cursor.fetchall()
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return data

    def get_failed_tasks(self):
        conn = self.cpool.getconn()
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            cursor.execute("SELECT job_id, status from task_history WHERE status = 'Failed' ")
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            raise e
        data = cursor.fetchall()
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return data

    #Metrics Functions

    def get_task_times_by_user(self, user_id):
        conn = self.cpool.getconn()
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            cursor.execute("SELECT job_id, status, cwl ->> 'label' AS label, status, created_at, completed_at from task_history WHERE user_id = %s ORDER BY created_at DESC", (user_id,))
        except psycopg2.Error as e:
            self.logger.error(crayons.red(e.pgerror))
            raise e
        data = cursor.fetchall()
        cursor.close()
        conn.commit()
        self.cpool.putconn(conn)
        return data