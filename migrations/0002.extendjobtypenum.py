from yoyo import step

step (
    "ALTER TYPE status ADD VALUE 'Aborting';",
    "DROP TYPE status; CREATE TYPE status as ENUM ('Running', 'Canceled', 'Failed', 'Succeeded', 'Submitted', 'Aborting');",
    ignore_errors='apply'
)