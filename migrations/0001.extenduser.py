from yoyo import step

step (
    "ALTER TABLE users ADD COLUMN is_enabled boolean NOT NULL DEFAULT true",
    "ALTER TABLE users DROP COLUMN is_enabled;",
    ignore_errors='apply'
)
