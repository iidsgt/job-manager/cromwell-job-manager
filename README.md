# Cromwell Manager API

## Pseudo Production Testing 

To test the full stack (Manager, Executor, HPC), run the following commands.

```bash
docker-compose -f slurm-docker-compose.yml build
docker-compose -f slurm-docker-compose.yml up
```

You can then access the swagger information at [http://localhost:8080/swagger](http://localhost:8080/swagger)

To submit a job you will need to hit the /v1/auth route with a payload below:

```json
{
    "username":"test",
    "password":"test"
}
```
This will return an access token that can then be included in the Authorization header to make a job submission.

To make this easier we have created jobtool which will automate some of this for submitting from the commandline. It can be installed locally:

```bash
pip install git+https://gitlab.com/iidsgt/job-manager/job-manager-cli.git  
```

Once installed, you will be asked to configure it on your first run, use the url http://localhost:8080 as the endpoint url.


## Required Environmental Variables

```
export ENVIRONMENT=development
export GITLAB_TOKEN=<gitlab_personal_access_token>
```

## Routing

As an extension to sanic we have devolped kaknuckles (pronunced ka-knuckles). This extenion takes in the base sanic app class as the first position argument & at least one route class as the second argument.

```py
def create_routes(app: "class Sanic" , *args: "class Route")
```
#### Example:
```py
from RouteClassExample import TestGroup
from sanic import Sanic

app = Sanic()

app = create_routes(app, TestGroup)
```
When creating a route group the top-level class must be named using camelcasing and must also have a base prop set. The base prop acts as the mount location for the route group.

*Note: A current limitation of knuckles is how it resolves url paths, a trailing slash should not be included on the base url prop*

```py
class TestGroup():
    base = "/v1"
```

Each inner class defines a single endpoint and must be prefixed with a double underscore (__).

These classes also need to be a subclass of the HTTPMethodView class included in Sanic.

**The subclass should not conain any url unsafe characters as the name will be lowercased to generate the route name.**

```py
class __TestRoute(HTTPMethodView):
        
        def get(self, request):
            return text("Hello {} and {}".format(name, test))
```

The functions inside of this class follow the standard sanic conventions that can be found [here](https://sanic.readthedocs.io/en/latest/sanic/class_based_views.html).

Each function requires accepting at least two arguments self and and request. Any additional arguments will be used to generate a dynamic route with parameters.

```py
class __TestRoute(HTTPMethodView):
        
        def get(self, request, name, test):
            return text("Hello {} and {}".format(name, test))
```
These parameters are surounded by angle brackets (\<param\>) in the url to indicate they accept values.

#### Example:
```py
from sanic.views import HTTPMethodView
from sanic.response import text

class TestGroup():
    base = "/v1"

    class __TestRoute(HTTPMethodView):
        
        def get(self, request, name, test):
            return text("Hello {} and {}".format(name, test))

# This creates a route with a get method at /v1/testgroup/testroute/<name>/<test>
```

## Extensions

In order to extended sanic programatically a functional approach should be used on the base class. The easiest way to extend sanic is by creating a function that accepts the sanic class as the first parameter and returns the mutated app class.

```py
def create_routes(app: "class Sanic" , *args: "class Route"):
    app.new_method = types.MethodType( newMethod, app )
    #or
    app.add_route(routeHandler, "/your_route_here")
    return app

app = create_routes(app, TestGroup)
```
*Note: It may be useful to use the dir() function on app prior to adding any new methods in order to avoid possible overloading that would sanic not to function*

```py
if new_method.__name__ in dir(app):
    #Error Here
```

## Error handling

### Error Classes

All custom errors should be created by creating a subclass of the error class in the exceptions.py file. 


If an \_\_init\_\_ function is specified in the class then it should accept one of two items as the second argument. 

* arg -> An arbitrary value that will be filled into a print statement
* fn -> A function being called that is the source of the error.

In the case of a function it is imperative that the inspect module is used like shown below to obtain the arguments expected of the function & \_\_name\_\_ is used to obtain the name of the function.

```py
class FunctionArgumentError(Error):
    def __init__(self, fn):
        print("An incorrect number of arguments was specified")
        print("Function %s expects %s" % (fn.__name__, inspect.getargspec(fn).args))
```

When creating an exception it is important to **exclude** sys.exit, this should be called immediately after the exception is raised in order to prevent non-critical errors from killing the thread.

#### Correct:

```py
class RouteNameError(Error):
    def __init__(self, arg):
        print("There is an issue with the name of %s" % (arg))
        pass
```

#### Incorrect:

```py
class RouteNameError(Error):
    def __init__(self, arg):
        self.logger.error("There is an issue with the name of %s" % (arg))
        sys.exit() # This should not be included
        pass
```

It is also stylistically important to fill parameters in the print string using the % operator instead of format. This allows for finner control of types presented in the string.

Additionally, the crayons package should be included in order to make error printing more intuitive. Errors should be printed in <span style="color:red">red</span>.


#### Example:
```py
import crayons
import inspect

class Error(Exception):
   """Base class for other exceptions"""
   pass

class RouteNameError(Error):
    def __init__(self, arg):
        self.logger.error(crayons.red("There is an issue with the name of %s" % (arg)))
        pass
```


## Variables

When creating a new variable that uses or modifies data from another variable stylistically it may be better to add an underscored to the original name of the variable and some information about the transformation.

```py
password = '12345'
password_hash = md5(password)
```

## Routing Rules

(Decorators and protected and scopes go here)

## API Definition

After some investigation it is useful to follow a similar spec provided by [GA4GH](https://https://github.com/ga4gh/task-execution-schemas).

Better documentation can be found with [Funnel](https://ohsu-comp-bio.github.io/funnel/docs/tasks/).

As we are using CWL it may require us to make some alterations or provide a second set of routes that support the API. 

### /tasks/

This route class accepts all task related operations.

To create a task a POST request is made to 

```json
"POST /v1/tasks"
{
  "name": "Hello world",
  "inputs": [{
    "url": "s3://funnel-bucket/hello.txt",
    "path": "/inputs/hello.txt"
  }],
  "outputs": [{
    "url": "s3://funnel-bucket/output.txt",
    "path": "/outputs/stdout"
  }],
  "executors": [{
      "image": "alpine",
      "command": ["cat", "/inputs/hello.txt"],
      "stdout": "/outputs/stdout"
  }]
}


# The response is a task ID:
"b85khc2rl6qkqbhg8vig"
```

## Notes

We probably should have the ability to be able to identify the location of the file based on the url prefix, i.e. s3:// pulls from amazon, sftp:// pulls from sftp etc.