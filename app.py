from utils import exceptions
from config.environment_helper import EnvUtil
from config.logging_configurator import LoggingConfigurator
import logging
import os
from route_classes.cromwell import Cromwell
from route_classes.jobs import Jobs
from route_classes.users import Users
from route_classes.files import Files
from route_classes.metrics import Metrics
import sys
from sanic import Sanic
from sanic.response import json
from sanic_cors import CORS, cross_origin
from sanic_openapi import swagger_blueprint
from sanic_jwt import Initialize
import sentry_sdk
from sentry_sdk.integrations.sanic import SanicIntegration
from sentry_sdk.integrations.logging import LoggingIntegration
from utils.auth import auth
from utils.db_interract import db_interact
from utils.serversidetasks import db_syncronization
from utils.sanicbuilder import create_routes



def init(app):
    ### Logging Setup
    base_dir = os.path.dirname(__file__) or "."
    LoggingConfigurator.set_up_logger(f"{base_dir}/config/logger.json")
    logger = logging.getLogger(__name__)
    EnvUtil.initialize(f"{base_dir}/config/environment.yml")
    #sentry_logging = LoggingIntegration(level=logging.DEBUG, event_level=logging.ERROR )

    ### Initialize Database
    db = db_interact()

    ### App & Route Initialization
    try:
        #sentry_sdk.init(EnvUtil.get("sentry_key"), 
        #integrations=[SanicIntegration(),sentry_logging],
        #environment=os.getenv("ENVIRONMENT"))
        #logger.info(f"sentry_sdk initialized: Environment: {os.getenv('ENVIRONMENT')}; DSN {EnvUtil.get('sentry_key')}")
        create_routes(app, Cromwell, Jobs, Files, Users, Metrics, params={'db':db}, logger=logger)

        app.add_task(db_syncronization(db))

        authenticate = auth(db)
        Initialize(app, 
                    authenticate=authenticate.authenticate, 
                    secret=EnvUtil.get('secret'),
                    url_prefix='/v1/auth',
                    retrieve_user=authenticate.getuser,
                    extra_verifications=[authenticate.check_api_key],
                    add_scopes_to_payload=authenticate.scope)

        CORS(app, automatic_options=True)
        
        @app.route('/')
        async def home(request):
            return json({'Status':'ok'})
        app.blueprint(swagger_blueprint)
    except Exception as e:
        logger.error(f'Fatal Exception: {e}')

if __name__ == '__main__':  # pragma: no cover
    app = Sanic()
    init(app)
    app.run(port=EnvUtil.get("running_port"), host='0.0.0.0')
    