FROM alpine:3.9

RUN apk add --no-cache openjdk8-jre-base curl
RUN curl -o /cromwell.jar https://github.com/broadinstitute/cromwell/releases/download/41/cromwell-41.jar

COPY cromwell-local.conf /cromwell-local.conf