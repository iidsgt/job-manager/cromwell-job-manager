FROM alpine:3.9

RUN apk add --no-cache postgresql git postgresql-contrib

RUN mkdir -p /var/run/postgresql && chown -R postgres:postgres /var/run/postgresql && chmod 2777 /var/run/postgresql

RUN postgresql-setup --initdb

EXPOSE 5432

CMD ["postgres"]

COPY Schema.sql

