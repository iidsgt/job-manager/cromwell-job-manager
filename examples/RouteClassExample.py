from sanic.views import HTTPMethodView
from sanic.response import text

class TestGroup():
    base = "/v1"

    class __TestRoute(HTTPMethodView):
        
        async def get(self, request, name, test):
            return text("Hello {} and {}".format(name, test))

    class __TestRoute2(HTTPMethodView):
        
        async def get(self, request, name, test5):
            return text("Hello {} and {}".format(name, test5))

    class __TestRoute3(HTTPMethodView):
        
        async def get(self, request, name, test2):
            return text("Hello {} and {}".format(name, test2))
