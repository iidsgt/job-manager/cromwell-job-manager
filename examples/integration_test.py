import requests

url = 'http://127.0.0.1:8080/v1/cromwell/'
#url = 'http://ec2-3-92-215-132.compute-1.amazonaws.com/v1/cromwell/'

print('Testing app....')
print('App is running!') if (requests.get(url + 'version').status_code) == 200  else print('App is not online.')
print('Cromwell is online and running!') if (requests.get(url + 'serverstatus').status_code) == 200  else print('Cromwell is not online.')
print('Testing Authentication....') 
print('Testing User Creation....')
print('User Creation Success') if (requests.post('http://127.0.0.1:8080/v1/users/createuser', json={"username":"test5","password":"test5","email":"mailfgfgf@mail.com"}).status_code) == 200 else print('User Creation Failed')
print('Auth Endpoint is Running') if (requests.post('http://127.0.0.1:8080/v1/auth', json={'username':'test', 'password':'test'}).status_code) == 200 else print('Auth Failed')
akey = requests.post('http://127.0.0.1:8080/v1/auth', json={'username':'test', 'password':'test'}).json()['access_token']
print(akey)
print(requests.get('http://127.0.0.1:8080/v1/users', headers={'Authorization': 'Bearer '+ akey}).json())
uid = [x for x in requests.get('http://127.0.0.1:8080/v1/users', headers={'Authorization': 'Bearer '+ akey}).json() if x['username'] == 'test5'][0]['user_id']
print('User Removal Success') if (requests.delete('http://127.0.0.1:8080/v1/users/removeuser', json={"user_id":uid}, headers={'Authorization': 'Bearer '+ akey}).status_code) == 200 else print('User Removal Failed')
print('Testing Job submission....')
print('Job submitted to cromwell sucessfully!') if (requests.post('http://127.0.0.1:8080/v1/jobs/submitjob', headers={'Authorization': 'Bearer '+ akey}, json={"workflowSource": "examples/echo.cwl", "workflow_inputs":"examples/message.yml"}).status_code) == 200  else print('Error while submitting job.')