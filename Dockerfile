FROM python:3.7
WORKDIR /code
RUN apt-get update
RUN mkdir -p /usr/share/man1 && mkdir -p /usr/share/man7
RUN apt-get install -y postgresql-client
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
EXPOSE 8080
CMD ["python", "app.py"]