if [ -f ./docker-singlecontainer-v1.zip ]; then
    rm docker-singlecontainer-v1.zip
fi

git archive -v -o docker-singlecontainer-v1.zip --format=zip HEAD
mkdir build
cd build/
unzip ../docker-singlecontainer-v1.zip
cd ..
cat env.yml > ./build/config/environment.yml
cp version.json ./build/version.json
cd build
zip -r ../docker-singlecontainer-v1.zip ./*
cd ..
rm -rf build/*
rm -rf build/.*
rm -r build
