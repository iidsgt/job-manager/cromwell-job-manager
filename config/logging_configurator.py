import logging
import os
import json
import logging.config


class LoggingConfigurator:

    def __init__(self): # pragma: nocover
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def set_up_logger(config_path: str):
        print(f"Initializing logger from {config_path}")
        with open(config_path, 'r') as json_file:
            logger_configuration = json.load(json_file)
            logging.config.dictConfig(logger_configuration)
            root_level = os.environ.get("LOG_LEVEL")
            if root_level:
                logging.getLogger().setLevel(root_level)
