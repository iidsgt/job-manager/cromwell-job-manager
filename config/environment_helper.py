import os, yaml, logging
from config.conversions import Conversions

DEFAULT_MISSING = None


class EnvUtil:

    environment = None
    environment_config = {}
    config_path = None
    config_dir = None

    @classmethod
    def initialize(cls, config_path: str):
        cls.config_path = config_path
        cls.logger = logging.getLogger(__name__)

        if os.environ.get('ENVIRONMENT'):
            cls.environment = os.environ.get('ENVIRONMENT')
        else:
            cls.logger.warning("Defaulting to 'development' configuration since ENVIRONMENT var is not set.")
            cls.environment = 'development'

        cls.environment = cls.environment.upper()
        cls.logger.info(f"Environment set to {cls.environment}")

        try:
            cls.logger.info(f"Initializing {cls.environment} environment configuration from {config_path}")
            with open(config_path, 'r') as yaml_file:
                cls.environment_config = yaml.safe_load(yaml_file)
                EnvUtil._uppercase_keys(cls.environment_config)
                cls.config_dir = os.path.dirname(config_path)
        except Exception as e:
            cls.logger.fatal(f"Failed to load environment.yml: {e}")
            exit(1)

    @staticmethod
    def _uppercase_keys(d: dict) -> None:
        orig_keys = list(d.keys())
        for k in orig_keys:
            v = d.get(k)
            if isinstance(v, dict):
                EnvUtil._uppercase_keys(v)
            del d[k]
            d[k.upper()] = v

    @classmethod
    def get(cls, name: str, default: any = DEFAULT_MISSING) -> any:
        try:
            name = name.upper()

            env_val = os.environ.get(name)
            if env_val is not None:
                return env_val

            if not cls.environment:
                raise KeyError('ENVIRONMENT is not configured')

            if cls.environment_config.get(cls.environment):
                env_config_val = cls.environment_config[cls.environment].get(name)
                if env_config_val is not None:
                    return env_config_val

            return default
        except AttributeError as e:  # pragma: no cover
            raise KeyError(f"Environment is not initialized: {e}")

