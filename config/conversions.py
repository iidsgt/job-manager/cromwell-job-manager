class Conversions:

    @staticmethod
    def to_bool(val: any) -> bool:
        return val in [True, 1, '1', 'True', 'T', 'true', 't', 'Yes', 'Y', 'yes', 'y']
