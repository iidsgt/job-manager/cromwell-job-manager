import crayons
import traceback
from utils.cromwell import Cromwell as cwm
from sanic_openapi import doc
from config.environment_helper import EnvUtil
from utils import exceptions
from sanic.views import HTTPMethodView
from sanic.response import text, json, raw, html
import logging
import os
import sys



class Cromwell():
    base = "/v1"

    @classmethod
    def init(self, params=None):
        self.logger = logging.getLogger(__name__)
        port = EnvUtil.get('cromwell_port')
        cromwell_domain = EnvUtil.get('cromwell_domain')
        cromwell_user = EnvUtil.get('cromwell_user') or ''
        cromwell_password = EnvUtil.get('cromwell_password') or ''
        cromwell_url = f'{cromwell_domain}:{port}'
        cromwell_config = {'cromwell_url': cromwell_url, 'username': cromwell_user, 'password': cromwell_password}
        try:
            self.cw = cwm(**cromwell_config)
        except Exception as e:
            self.logger.error(crayons.red(e))

        self.db = params['db']

    class __ServerStatus(HTTPMethodView):
        
        @doc.tag("Cromwell")
        @doc.summary("Gets the status of the cromwell server.")
        async def get(self, request):
            try:
                status = await Cromwell.cw.server_is_running()
                print(status)
                Cromwell.logger.info(status)
                if status: status = 'Online'
            except Exception as e:
                Cromwell.logger.error(traceback.format_exc())
                status = 'Offline'
                return json({'Error': traceback.format_exc()}, status=500)
            return text({'server_status': status})

    class __ServerSwagger(HTTPMethodView):
        
        @doc.tag("Cromwell")
        @doc.summary("Returns the cromwell swagger documentation.")
        async def get(self, request):
            return html(Cromwell.cw.swagger())


    class __Version(HTTPMethodView):
        
        @doc.tag("Cromwell")
        @doc.summary("Gets the version of the cromwell server")
        async def get(self, request):
            version = Cromwell.cw.version()

            return raw((await version).content, content_type="application/json")


    class __Healthcheck(HTTPMethodView):

        @doc.tag("Cromwell")    
        async def get(self, request):
            environment = os.getenv('ENVIRONMENT')
            return json({'Status': 'Healthy',
                            'environment': environment
                })
