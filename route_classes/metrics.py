import crayons
from utils.cromwell import Cromwell as cwm
import datetime
from sanic_openapi import doc
from config.environment_helper import EnvUtil
from utils import exceptions
from sanic.views import HTTPMethodView
import jwt
import json as j
import logging
import os
from sanic_jwt.decorators import protected
from sanic.response import text, json
import sys
import traceback
from ruamel.yaml import YAML


class Metrics():
    base = "/v1"

    @classmethod
    def init(self, params=None):
        self.logger = logging.getLogger(__name__)
        self.cromwell_output_dir = EnvUtil.get("cromwell_output_dir")
        port = EnvUtil.get('cromwell_port')
        cromwell_domain = EnvUtil.get('cromwell_domain')
        cromwell_user = EnvUtil.get('cromwell_user')
        cromwell_password = EnvUtil.get('cromwell_password')
        cromwell_url = f'{cromwell_domain}:{port}'
        cromwell_config = {'cromwell_url': cromwell_url, 'username': cromwell_user, 'password': cromwell_password}
        try:
            self.cw = cwm(**cromwell_config)
        except Exception as e:
            self.logger.error(crayons.red(e))
        self.yaml = YAML(typ='safe')

        self.db = params['db']

    @classmethod
    def check_if_allowed(self, user_id, workflow_id):
        try:
            wf_data = self.db.get_task_information(workflow_id)
            if (user_id == wf_data['user_id']):
                return True
            else:
                raise Exception("Not Allowed")
        except Exception as e:
            raise e

    @classmethod
    def get_jobs_status_count(self, user_id):
        try:
            tasks = self.db.get_tasks_by_user(user_id)
        except Exception as e:
            raise(e)

        metrics = {
            "Failed": 0,
            "Running": 0,
            "Succeeded": 0,
            "Submitted": 0
        }

        for task in tasks:
            if task['status'] == 'Failed':
                metrics['Failed'] += 1
            if task['status'] == 'Succeeded':
                metrics['Succeeded'] += 1
            if task['status'] == 'Running':
                metrics['Running'] += 1
            if task['status'] == 'Submitted':
                metrics['Submitted'] += 1

        return metrics

    @classmethod
    def get_job_runtimes(self, user_id):
        try:
            tasks = self.db.get_task_times_by_user(user_id)
        except Exception as e:
            raise(e)

        updated_tasks = []

        for task in tasks:
            t = {
                'job_id': '',
                'job_label': '',
                'time_delta': '',
                'created_at': '',
                'completed_at': ''
            }

            t['created_at'] = task['created_at']
            t['completed_at'] = task['completed_at'] if task['completed_at'] else ''
            t['job_id'] = task['job_id']
            t['job_label'] = task['label']
            t['status'] = task['status']

            try:
                time_delta = task['completed_at'] - task['created_at']
                t['time_delta'] = time_delta
            except Exception as e:
                pass

            updated_tasks.append(t)

        return updated_tasks

    class __root(HTTPMethodView):
        decorators = [protected()]
        
        @doc.tag("Metrics")
        @doc.summary("Get dashboard metrics for the current user.")

        async def get(self, request):
            try:
                user_id = jwt.decode(request.headers.get('Authorization').split('Bearer ')[1], EnvUtil.get('secret'),
                                        algorithm='HS256')['user_id']

                jobs_count = Metrics.get_jobs_status_count(user_id)
                jobs_time = Metrics.get_job_runtimes(user_id)

                metrics = {
                    "jobs_count":jobs_count,
                    "jobs_time":jobs_time
                }

                return json(metrics)

            except Exception as e:
                Metrics.logger.error(traceback.format_exc())
                return json({'Error': e.args}, status=500)


    

            
