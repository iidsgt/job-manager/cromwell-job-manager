import crayons
from utils.cromwell import Cromwell as cwm
import datetime
import dateparser
from sanic_openapi import doc
from config.environment_helper import EnvUtil
from utils import exceptions
from flask import json
from sanic.views import HTTPMethodView
import jwt
import json as j
import logging
import os
from sanic_jwt.decorators import protected
from sanic.response import text, json
import sys
import traceback
from ruamel.yaml import YAML
import platform


class Jobs():
    base = "/v1"

    @classmethod
    def init(self, params=None):
        self.logger = logging.getLogger(__name__)
        self.cromwell_output_dir = EnvUtil.get("cromwell_output_dir")
        port = EnvUtil.get('cromwell_port')
        cromwell_domain = EnvUtil.get('cromwell_domain')
        cromwell_user = EnvUtil.get('cromwell_user')
        cromwell_password = EnvUtil.get('cromwell_password')
        cromwell_url = f'{cromwell_domain}:{port}'
        cromwell_config = {'cromwell_url': cromwell_url, 'username': cromwell_user, 'password': cromwell_password}
        try:
            self.cw = cwm(**cromwell_config)
        except Exception as e:
            self.logger.error(crayons.red(e))
        self.yaml = YAML(typ='safe')

        self.db = params['db']

    @classmethod
    def check_if_allowed(self, user_id, workflow_id):
        try:
            wf_data = self.db.get_task_information(workflow_id)
            if (user_id == wf_data['user_id']):
                return True
            else:
                raise Exception("Not Allowed")
        except Exception as e:
            raise e

    class __root(HTTPMethodView):
        decorators = [protected()]
        
        @doc.tag("Jobs")
        @doc.summary("Returns the tasks submitted by the user")
        async def get(self, request):
            try:
                user_id = jwt.decode(request.headers.get('Authorization').split('Bearer ')[1], EnvUtil.get('secret'),
                                     algorithm='HS256')['user_id']
                data = Jobs.db.get_tasks_by_user(user_id)
                return json(data)
            except Exception as e:
                Jobs.logger.error(traceback.format_exc())
                return json({'Error': e.args}, status=500)

    class __SubmitJob(HTTPMethodView):
        decorators = [protected()]

        @doc.tag("Jobs")
        @doc.summary("Submits a job to the job manager.")
        @doc.consumes(doc.String(name="Authorization"), location="header", required=True)
        @doc.consumes(doc.File(description="CWL workflow file", name="workflow"), location="file", required=True)
        @doc.consumes(doc.File(description="File that specifies path to inputs", name="inputs"), location="file", required=True)
        @doc.consumes(doc.File(description="Mapping file that can be used to connect inputs to paths", name="mappings"), location="file", required=True)
        async def post(self, request):
            """Post job

            :param request: workflow and inputs binaries

            :return json result of post
            """
            try:
                cwl = request.files.get('workflow')
                inputs = request.files.get('inputs')
                mappings = request.files.get('mappings', {})

                user_id = jwt.decode(request.headers.get('Authorization').split('Bearer ')[1], EnvUtil.get('secret'),
                                     algorithm='HS256')['user_id']

                inputs_dict = Jobs.yaml.load(inputs.body)
                cwl_dict = Jobs.yaml.load(cwl.body)
                parsed_cwl = j.dumps(cwl_dict)
                parsed_inputs = j.dumps(inputs_dict)

                if len(mappings) > 0:
                    mappings = Jobs.yaml.load(mappings.body)
                    for item in inputs_dict:
                        if type(inputs_dict[item]) == dict:
                            if inputs_dict[item]['class'] == "File":
                                inputs_dict[item]['path'] = mappings[inputs_dict[item]['path']]
                    parsed_inputs = j.dumps(inputs_dict)

                files = {
                    'workflowSource': cwl.body,
                    'workflowInputs': parsed_inputs,
                    'workflowType': 'CWL'
                }

                if Jobs.cromwell_output_dir:
                    files['workflowOptions'] = j.dumps({"final_workflow_outputs_dir":Jobs.cromwell_output_dir})

                result = await Jobs.cw.submit(files)
                Jobs.logger.info("Submitted %s" % (result['status']))
                
                if (result['status'] == 'fail'):
                    raise Exception(result["message"])

                db_body = {
                    'cwl': parsed_cwl,
                    'inputs': parsed_inputs,
                    'job_id': result['id'],
                    'status': result['status'],
                    'mapping': j.dumps(mappings),
                    'user_id': user_id,
                    'created_at': datetime.datetime.utcnow().isoformat()
                }

                Jobs.db.create_task(**db_body)

                # Beautify output to return to request endpoint
                db_body['mapping'] = mappings
                db_body['cwl'] = cwl_dict
                db_body['inputs'] = inputs_dict

                return json(db_body)
            except Exception as e:
                Jobs.logger.error(traceback.format_exc())
                return json({'Error': e.args}, status=500)

    class __GetStatus(HTTPMethodView):
        decorators = [protected()]
        
        @doc.tag("Jobs")
        @doc.summary("Get status of a submitted workflow.")
        async def get(self, request, workflow_id):
            try:
                user_id = jwt.decode(request.headers.get('Authorization').split('Bearer ')[1], EnvUtil.get('secret'),
                                     algorithm='HS256')['user_id']
                Jobs.check_if_allowed(user_id, workflow_id)
                status = Jobs.db.get_task_information(workflow_id)['status']
                return json({'Status': status})
            except Exception as e:
                Jobs.logger.error(traceback.format_exc())
                return json({'Error': traceback.format_exc()}, status=500)

    class __GetBatchStatus(HTTPMethodView):
        decorators=[protected()]

        @doc.tag("Jobs")
        @doc.summary("Get status for a batch of job_ids")
        @doc.consumes(doc.String(name="Authorization"), location="header", required=True)
        @doc.consumes(doc.JsonBody({"jobs": doc.List("A List of job ids to get information about.")}), location="body")
        async def post(self, request):
            try:
                user_id = jwt.decode(request.headers.get('Authorization').split('Bearer ')[1], EnvUtil.get('secret'),
                                     algorithm='HS256')['user_id']
                req_body = request.json
                job_list = req_body.get("jobs")
                
                batch_status = Jobs.db.get_batch_task_information(job_ids=job_list, user_id=user_id)

                return json({'jobs': batch_status})
            except Exception as e:
                Jobs.logger.error(traceback.format_exc())
                return json({'Error': traceback.format_exc()}, status=500)

    class __CancelJob(HTTPMethodView):
        decorators = [protected()]

        @doc.tag("Jobs")
        @doc.summary("Cancels a currently running job.")
        @doc.consumes(doc.JsonBody({"job_id": doc.String("The unique id of the job submitted")}), location="body")
        async def put(self, request):
            try:
                user_id = jwt.decode(request.headers.get('Authorization').split('Bearer ')[1], EnvUtil.get('secret'),
                                     algorithm='HS256')['user_id']
                req_body = request.json
                job_id = req_body.get("job_id")

                Jobs.check_if_allowed(user_id, job_id)
                data = await Jobs.cw.abort_workflow(job_id)

                if (Jobs.db.update_task(data['status'], job_id) == 1):
                    return json({'Status': data['status']})
                else:
                    raise Exception("Job Collision Error")

            except Exception as e:
                Jobs.logger.error(traceback.format_exc())
                return json({'Error': e.args}, status=500)

    class __GetMetadata(HTTPMethodView):
        decorators = [protected()]

        @doc.tag("Jobs")
        @doc.summary("Gets the cromwell level metadata for the workflow run.")
        async def get(self, request, job_id):
            try:
                user_id = jwt.decode(request.headers.get('Authorization').split('Bearer ')[1], EnvUtil.get('secret'),
                                     algorithm='HS256')['user_id']
                
                Jobs.check_if_allowed(user_id, job_id)
                data = await Jobs.cw.metatdata(job_id)
                return json(data)

            except Exception as e:
                Jobs.logger.error(traceback.format_exc())
                return json({'Error': e.args}, status=500)

    class __BuildInfo(HTTPMethodView):
        decorators = [protected()]

        @doc.tag("Jobs")
        @doc.summary("Gets Build info for Manager.")
        async def get(self, request):
            try:
                path = os.path.join(os.path.dirname(__file__), '..', 'version.json')
                with open(path, 'r') as json_file:
                    build_info = j.load(json_file)
                    build_info['environment'] = EnvUtil.environment
                    build_info['python_version'] = platform.python_version()
                    build_info['manager_version'] = 'BETA'
                    now = datetime.datetime.now()
                    commit = dateparser.parse(build_info['build_time'])
                    build_info['last_updated'] = days_hours_minutes(now - commit)
            except Exception as e:
                Jobs.logger.error(traceback.format_exc())
                return json({'Error': e}, status=500)

            return json(build_info)


def days_hours_minutes(td):
    return f"{td.days} days, {td.seconds//3600} hours, {(td.seconds//60)%60} minutes ago...'"
