import crayons
from utils.cromwell import Cromwell as cwm
from config.environment_helper import EnvUtil
from sanic.views import HTTPMethodView
from sanic.response import json, raw
from utils.gitlab_accessor import gitlab_interact
from utils.file_manager import FileManager
import logging
import traceback



class Files():
    base = "/v1"

    @classmethod
    def init(self, params=None):
        self.logger = logging.getLogger(__name__)
        port = EnvUtil.get('cromwell_port')
        cromwell_domain = EnvUtil.get('cromwell_domain')
        cromwell_user = EnvUtil.get('cromwell_user')
        cromwell_password = EnvUtil.get('cromwell_password')
        cromwell_url = f'{cromwell_domain}:{port}'
        cromwell_config = {'cromwell_url': cromwell_url, 'username': cromwell_user, 'password': cromwell_password}
        try:
            self.cw = cwm(**cromwell_config)
        except Exception as e:
            self.logger.error(crayons.red(e))
        self.db = params['db']

    class __GetOutputFiles(HTTPMethodView):

        async def get(self, request, workflow_id):
            try:
                outputs = Files.cw.output_files(workflow_id)
                return json({"Outputs": outputs})
            except Exception as e:
                Files.logger.error(traceback.format_exc())

    # class __GetLocalFiles(HTTPMethodView):
        
    #     async def get(self, request):
    #         try:
    #             files = FileManager().list_files()
    #             return json({"LocalFiles": files})
    #         except Exception as e:
    #             Files.logger.error(e)
    #             return json({'Error': e.args}, status=500)


    # class __GetCWLs(HTTPMethodView):

    #     async def get(self, request, cwl_type):
    #         try:
    #             files = gitlab_interact().get_files(cwl_type.capitalize())
    #             return json(files)
    #         except Exception as e:
    #             Files.logger.error(e)
    #             return json({'Error': e.args}, status=500)


    # class __DownloadCWL(HTTPMethodView):

    #     async def get(self, request):
    #         try:
    #             files = gitlab_interact().download_files(f"{request.args['type'][0]}%2F{request.args['file_name'][0]}")
    #             return raw(files)
    #         except Exception as e:
    #             Files.logger.error(e)
    #             return json({'Error': e.args}, status=500)

