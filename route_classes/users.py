import crayons
from config.environment_helper import EnvUtil
from sanic_openapi import doc
from sanic.views import HTTPMethodView
from sanic_jwt.decorators import protected, scoped
from sanic.response import text, json
from hashlib import sha256
import logging 
from uuid import UUID, uuid4
from utils import exceptions
import jwt


class Users():
    base = "/v1"

    @classmethod
    def init(self, params=None):
        self.logger = logging.getLogger(__name__)
        self.db = params['db']

    class __root(HTTPMethodView):
        decorators = [protected(), scoped('admin')]

        @doc.tag("Users")
        @doc.summary("Returns data for all users (requires admin rights).")
        async def get(self, request):
            try:
                users = Users.db.list_all_users()
                return json(users)
            except Exception as e:
                Users.logger.error(e)
                return json({'Error': e.args}, status=500)

    class __getuser(HTTPMethodView):
        decorators = [protected(), scoped('admin')]

        @doc.tag("Users")
        @doc.summary("Returns user information (requires admin rights)")
        async def get(self, request, user_id=None):
            try:
                uuid = UUID(user_id, version=4)
            except ValueError as e:
                Users.logger.error(e)
                return json({'Error': 'Invalid user ID'}, status=500)
            try:
                user = Users.db.get_user(str(uuid))[0]
                return json(user)
            except Exception as e:
                Users.logger.error(e)
                return json({'Error': e.args}, status=500)

    class __createuser(HTTPMethodView):
        decorators = [protected(), scoped('admin')]

        @doc.tag("Users")
        @doc.summary("Creates a new user (requires admin rights).")
        @doc.consumes(doc.JsonBody({"username": doc.String("A username for the new user."), 
                                    "password": doc.String("A password for the new user."),
                                    "email": doc.String("Email for the new user.")}), location="body")
        async def post(self, request):
            req_body = request.json
            password = sha256(bytes(req_body.get('password'), encoding='utf-8')).hexdigest()
            db_body = {
                'username': req_body.get("username"),
                'password': password,
                'email': req_body.get("email"),
                'role':'user',
            }
            try:
                uuid = Users.db.create_user(**db_body)
                return json({'Status':'User Created', 'user_id':uuid})

            except Exception as e:
                Users.logger.error(e)
                return json({'Error': e.args}, status=500)

    class __removeuser(HTTPMethodView):
        decorators = [protected(), scoped('admin')]

        @doc.tag("Users")
        @doc.summary("Removes a user (requires admin rights).")
        @doc.consumes(doc.JsonBody({"user_id": doc.String("The id of the user")}), location="body")
        async def delete(self, request):
            req_body = request.json
            try:
                uuid = UUID(req_body.get('user_id'), version=4)
            except ValueError as e:
                Users.logger.error(e)
                return json({'Error': ['Invalid user ID']}, status=500) #I want this to all be captured below but this produces a specific type of exception
            try:
                if (Users.db.remove_user(str(uuid)) == 1):
                    return json({'Status':'User Removed'})
                else:
                    raise Exception("User Not Found")
            except Exception as e:
                Users.logger.error(e)
                return json({'Error': e.args}, status=500)

    class __addapikey(HTTPMethodView):
        decorators = [protected(), scoped('admin')]

        @doc.tag("Users")
        @doc.summary("Creates an API key for a user (requires admin rights).")
        @doc.consumes(doc.JsonBody({"user_id": doc.String("The id of the user")}), location="body")
        async def post(self, request):
            req_body = request.json
            try:
                uuid = UUID(req_body.get('user_id'), version=4)
            except ValueError as e:
                Users.logger.error(e)
                return json({'Error': ['Invalid user ID']}, status=500)
            try:
                user = Users.db.get_user(str(uuid))[0]
                
                secret = str(uuid4())

                payload = {
                    "user_id": str(uuid),
                    "scopes": [user["role"]],
                    "key": secret
                }
                key = jwt.encode(payload=payload, algorithm="HS256", key=EnvUtil.get("secret"))

                if (Users.db.set_api_key_user(str(uuid), secret) == 1):
                    return json({"key":key})
                else:
                    raise Exception("User Not Found")
            except Exception as e:
                Users.logger.error(e)
                return json({'Error': e.args}, status=500)

    class __removeapikey(HTTPMethodView):
        decorators = [protected(), scoped('admin')]

        @doc.tag("Users")
        @doc.summary("Deletes an API key for a user (requires admin rights).")
        @doc.consumes(doc.JsonBody({"user_id": doc.String("The id of the user")}), location="body")
        async def delete(self, request):
            req_body = request.json
            try:
                uuid = UUID(req_body.get('user_id'), version=4)
            except ValueError as e:
                Users.logger.error(e)
                return json({'Error': ['Invalid user ID']}, status=500)
            try:
                if (Users.db.remove_api_key_user(str(uuid)) == 1):
                   return json({'Status':'API Key Revoked'})
                else:
                    raise Exception("User Not Found")
            except Exception as e:
                Users.logger.error(e)
                return json({'Error': e.args}, status=500)
        